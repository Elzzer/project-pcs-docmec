﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace projectPCS
{
    public partial class customer : Form
    {
        OracleConnection conn = new OracleConnection("Data Source=orcl;User ID=project;Password=123;");
        string username = "";
        int pilihMenu = 0;
        public customer(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        private void customer_Load(object sender, EventArgs e)
        {
            this.Size = new Size(800, 800);
            this.Location = new Point(0, 0);
            this.BackgroundImage = Image.FromFile("customer.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            dataGridView1.Location = new Point(81, 164);
            dataGridView1.Size = new Size(640, 430);

            //buat back/logout
            panel1.Size = new Size(30, 39);
            panel1.Location = new Point(734, 42);
            panel1.BackColor = Color.Transparent;

            //buat poin
            label2.Location = new Point(620,620);
            label2.Font = new Font("arial", 13);
            label2.ForeColor = Color.White;
            label2.BackColor = Color.Transparent;
            //buat nama username
            string usernameDisplay = username.ToUpper();
            label1.Text = "";
            for (int i = 0; i < usernameDisplay.Length; i++)
            {
                label1.Text += usernameDisplay[i];
                label1.Text += " ";
            }
            label1.ForeColor = Color.FromArgb(222, 178, 59);
            label1.Location = new Point(400, 67);
            label1.Font = new Font("arial", 36,FontStyle.Bold);
            label1.BackColor = Color.Transparent;

            isiDGV();
        }

        public void isiDGV() {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select h.nota_jual,b.nama_barang, h.tanggal, d.qty, (b.harga*d.qty) as total from barang b, h_jual h, d_jual d where b.id_barang = d.id_barang and d.nota_jual = h.nota_jual and h.username_cust = '" + username + "' order by 1", conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "dgvCust");

            dataGridView1.DataSource = ds.Tables["dgvCust"];

            cmd = new OracleCommand("select poin from account where username='" + username + "'", conn);
            string temp = cmd.ExecuteScalar().ToString();
            label2.Text = "Poin : " + temp;
            conn.Close();

        }

        public void warnaDGV() {
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (i != 0)
                {
                    if (dataGridView1.Rows[i - 1].Cells[0].Value.ToString() == dataGridView1.Rows[i].Cells[0].Value.ToString())
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = dataGridView1.Rows[i - 1].DefaultCellStyle.BackColor;
                    }
                    else if (dataGridView1.Rows[i - 1].Cells[0].Value.ToString() != dataGridView1.Rows[i].Cells[0].Value.ToString())
                    {
                        if (dataGridView1.Rows[i - 1].DefaultCellStyle.BackColor == Color.Yellow) dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                        else dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                    }
                }
                else dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
            }
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 1)
            {
                pilihMenu = 1;
                this.Invalidate();
            }
        }

        private void customer_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 0)
            {
                pilihMenu = 0;
                this.Invalidate();
            }
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {// tombol LOGOUT
            ((Form1)MdiParent).login();
            this.Close();
        }

        private void customer_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush = new SolidBrush(Color.FromArgb(64, Color.Gray));
            if (pilihMenu == 1) g.FillRectangle(brush, 734, 42, 34, 39);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            warnaDGV();
        }
    }
}
