﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace projectPCS
{
    public partial class cashier : Form
    {
        int pilihMenu = 0;
        int hargaTotal = 0;
        OracleConnection conn = new OracleConnection("Data Source=sby;User ID=project;Password=123;");
        List<int> listHarga = new List<int>(); //buat tampung harga setiap barang pada supplier
        List<int> listHargaBeli = new List<int>(); //untuk tahu setiap barang harganya berapa pada listbox
        List<string> listIdBarang = new List<string>(); //untuk gabungin bila beli terpisah
        List<string> listNamaBarang = new List<string>(); //untuk gabungin bila beli terpisah(nama) dan untuk deletenya
        int[,] listQuantity = new int[100, 3]; //untuk tahu total yg dibeli dr idbarang tsb | [0] box [1]strip [2]biji
        string username = "";
        bool mulai = false; //buat harga biar g error
        public cashier(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        private void customer_Load(object sender, EventArgs e)
        {
            this.Size = new Size(800, 647);
            this.Location = new Point(0, 0);
            this.BackgroundImage = Image.FromFile("cashier.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            //inisialisasi listquantity
            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    listQuantity[i, j] = 0;
                }
            }

            //buat inputan
            comboBox1.Location = new Point(182, 206);
            comboBox1.Size = new Size(223, 24);
            comboBox1.Font = new Font("arial", 11);
            textBox1.Location = new Point(182, 255);
            textBox1.Size = new Size(223, 24);
            textBox1.Font = new Font("arial", 11);
            numericUpDown1.Location = new Point(182, 304);
            numericUpDown1.Size = new Size(100, 24);
            numericUpDown1.Font = new Font("arial", 11);
            //combobox3 buat tipe obat box atau biji
            comboBox3.Location = new Point(300, 304);
            comboBox3.Size = new Size(107, 24);
            comboBox3.Font = new Font("arial", 11);

            comboBox2.Location = new Point(182, 353);
            comboBox2.Size = new Size(223, 24);
            comboBox2.Font = new Font("arial", 11);
            numericUpDown2.Location = new Point(182,403);
            numericUpDown2.Size = new Size(223,24);
            numericUpDown2.Font = new Font("arial", 11);
            listBox1.Location = new Point(436, 206);
            listBox1.Size = new Size(260,225);

            //textbox2 buat alamat bila delivery
            textBox2.Location = new Point(182, 472);
            textBox2.Size = new Size(223,24);
            textBox2.Font = new Font("arial", 11);

            //radio button pickup or delivery
            radioButton1.Location = new Point(193,443);
            radioButton2.Location = new Point(284, 443);
            radioButton1.BackColor = Color.Transparent;
            radioButton2.BackColor = Color.Transparent;

            //buat + dan -
            panel1.Location = new Point(632, 436);
            panel1.Size = new Size(23, 23);
            panel2.Location = new Point(676, 436);
            panel2.Size = new Size(23, 23);
            panel1.BackColor = Color.Transparent;
            panel2.BackColor = Color.Transparent;
            //buat tombol checkout
            panel3.Location = new Point(537, 471);
            panel3.Size = new Size(157,22);
            panel3.BackColor = Color.Transparent;
            //buat tombol logout
            panel4.Location = new Point(717, 31);
            panel4.Size = new Size(50, 50);
            panel4.BackColor = Color.Transparent;

            //buat subtotal
            label2.Location = new Point(437, 439);
            label2.Font = new Font("arial", 16);
            label2.Text = "";
            label2.BackColor = Color.Transparent;
            //buat judul
            label1.Location = new Point(300, 90);
            label1.Font = new Font("arial", 33,FontStyle.Bold);
            label1.BackColor = Color.Transparent;
            label1.ForeColor = Color.FromArgb(222, 178, 59);
            label1.Text = "";
            for (int i = 0; i < username.Length; i++)
            {
                if (i == 0) label1.Text += username[i].ToString().ToUpper();
                else label1.Text += username[i].ToString();
                label1.Text += " ";
            }
            isiCb();
            radioButton1.Checked = true;
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            comboBox1.SelectedIndex = -1; mulai = true;
            comboBox2.SelectedIndex = -1;
        }

        public void isiCb() {
            //isi cb Barang
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from barang",conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();

            da.Fill(ds, "listBarang");
            comboBox1.DataSource = ds.Tables["listBarang"];
            comboBox1.DisplayMember = "nama_barang";
            comboBox1.ValueMember = "id_barang";
            conn.Close();

            //isi cbCustomer
            conn.Open();
            cmd = new OracleCommand("select * from account where status='C'", conn);
            da = new OracleDataAdapter(cmd);
            ds = new DataSet();

            da.Fill(ds, "listAccount");
            comboBox2.DataSource = ds.Tables["listAccount"];
            comboBox2.DisplayMember = "username";
            comboBox2.ValueMember = "poin";
            conn.Close();

            conn.Open();
            listHarga.Clear();
            cmd = new OracleCommand("select * from barang", conn);
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                listHarga.Add(Convert.ToInt32(reader["harga"]));
            }
            conn.Close();

            //isi combobox 3
            comboBox3.Items.Add("box");
            comboBox3.Items.Add("strip");
            comboBox3.Items.Add("biji");
            comboBox3.SelectedIndex = 0;
        }

        public void isiListHarga() {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from barang", conn);
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                listHarga.Add(Convert.ToInt32(reader["harga"]));
            }
            conn.Close();
        }
        
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) textBox2.Enabled = false;
            else textBox2.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) textBox2.Enabled = false;
            else textBox2.Enabled = true;
        }

        private void customer_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.X + " " + e.Y);
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 1)
            {
                pilihMenu = 1;
                this.Invalidate();
            }
        }

        private void panel2_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 2)
            {
                pilihMenu = 2;
                this.Invalidate();
            }
        }

        private void panel3_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 3)
            {
                pilihMenu = 3;
                this.Invalidate();
            }
        }

        private void panel4_MouseHover(object sender, EventArgs e)
        {//buat logout
            if (pilihMenu != 4)
            {
                pilihMenu = 4;
                this.Invalidate();
            }
        }

        private void customer_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 0)
            {
                pilihMenu = 0;
                this.Invalidate();
            }
        }

        private void customer_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush = new SolidBrush(Color.FromArgb(64, Color.Gray));
            if (pilihMenu == 1) g.FillEllipse(brush, 632, 436, 23, 23);
            else if (pilihMenu == 2) g.FillEllipse(brush, 676, 436, 23, 23);
            else if (pilihMenu == 3) g.FillRectangle(brush, 537, 471, 157, 22);
            else if (pilihMenu == 4) g.FillRectangle(brush, 717, 31, 50, 50);
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {//tambah pesanan / tambah listbox
            listBox1.Items.Add(comboBox1.Text + "-" + numericUpDown1.Value + "-" + comboBox3.Text);
            if (comboBox3.Text == "box")
            {
                listHargaBeli.Add(Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex] * 6 * 6);
                hargaTotal += Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex] * 6 * 6;
            }
            else if (comboBox3.Text == "strip")
            {
                listHargaBeli.Add(Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex] * 6);
                hargaTotal += Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex] * 6;
            }
            else
            {
                listHargaBeli.Add(Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex]);
                hargaTotal += Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex];
            }
            //listHargaBeli.Add(Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex]);
            //hargaTotal += Convert.ToInt32(numericUpDown1.Value) * listHarga[comboBox1.SelectedIndex];
            label2.Text = "Rp. " + hargaTotal.ToString() + ",-";

            bool cekAda = false;
            for (int i = 0; i < listIdBarang.Count; i++)
            {
                if (listIdBarang[i] == comboBox1.SelectedValue.ToString())
                {
                    cekAda = true;                               
                    if(comboBox3.Text == "box") listQuantity[i,0] += Convert.ToInt32(numericUpDown1.Value);
                    else if (comboBox3.Text == "strip") listQuantity[i,1] += Convert.ToInt32(numericUpDown1.Value);
                    else if (comboBox3.Text == "biji") listQuantity[i,2] += Convert.ToInt32(numericUpDown1.Value);
                    break;
                }
            }
            if (!cekAda)
            {
                listIdBarang.Add(comboBox1.SelectedValue.ToString());
                if (comboBox3.Text == "box") listQuantity[listIdBarang.Count-1,0] = Convert.ToInt32(numericUpDown1.Value);
                else if (comboBox3.Text == "strip") listQuantity[listIdBarang.Count - 1, 1] = Convert.ToInt32(numericUpDown1.Value);
                else if (comboBox3.Text == "biji") listQuantity[listIdBarang.Count - 1, 2] = Convert.ToInt32(numericUpDown1.Value);
                listNamaBarang.Add(comboBox1.Text);
            }

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {//batal pesanan / hapus listbox
            if (listBox1.SelectedIndex != -1)
            {
                string[] temp = listBox1.SelectedItem.ToString().Split('-');
                for (int i = 0; i < listIdBarang.Count; i++)
                {
                    if (listNamaBarang[i] == temp[0])
                    {
                        if (comboBox3.Text == "box") listQuantity[i, 0] -= Convert.ToInt32(temp[1]);
                        else if (comboBox3.Text == "strip") listQuantity[i, 1] -= Convert.ToInt32(temp[1]);
                        else if (comboBox3.Text == "biji") listQuantity[i, 2] -= Convert.ToInt32(temp[1]);
                        break;
                    }
                }

                //MessageBox.Show(listBox1.SelectedIndex + "");
                hargaTotal -= listHargaBeli[listBox1.SelectedIndex];
                listHargaBeli.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                label2.Text = "Rp. " + hargaTotal.ToString() + ",-";
            }
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {//buat checkout
            if (listBox1.Items.Count != 0) {
                int temp = 0;
                for (int i = 0; i < listHargaBeli.Count; i++)
                {
                    temp += listHargaBeli[i];
                }
                int discountPoint = Convert.ToInt32(numericUpDown2.Value) * 2000;
                //MessageBox.Show("discount point : " + discountPoint);
                int potongan = temp - discountPoint;
                //MessageBox.Show("total bayar : " + potongan);
                int checkDelivery = -1;
                if (radioButton1.Checked) checkDelivery = 0;
                else checkDelivery = 1;
                

                conn.Open();
                //MessageBox.Show("total bayar msk database : " + potongan);
                //OracleCommand cmd = new OracleCommand("insert into h_jual values('','" + comboBox2.Text.ToString() + "','" + username + "',to_date('" + DateTime.Now.ToShortDateString() + "', 'DD-MM-YYYY '),'" + checkDelivery +"','" + textBox2.Text + "'," + potongan + ")", conn);
                OracleCommand cmd = new OracleCommand("insert into h_jual values('','" + comboBox2.Text + "','" + username + "',to_date('" + DateTime.Now.ToShortDateString().ToString() + "','dd-mm-yyyy'),'" + checkDelivery + "','" + textBox2.Text + "'," + potongan + ")", conn);
                //cmd.Parameters.Add(":harga", temp);
                cmd.ExecuteNonQuery();
                conn.Close();
                //MessageBox.Show("total bayar setelah database : " + potongan);

                conn.Open();
                int tambahanPoin = temp / 20000;
                int poinAccount = Convert.ToInt32(comboBox2.SelectedValue) - Convert.ToInt32(numericUpDown2.Value) + tambahanPoin;
                cmd = new OracleCommand("update account set poin=:poin where username='" + comboBox2.Text + "'",conn);
                cmd.Parameters.Add(":poin", poinAccount);
                cmd.ExecuteNonQuery();

                OracleTransaction ot = conn.BeginTransaction();
                try
                {
                    cmd = new OracleCommand("select max(nota_jual) from h_jual", conn);
                    string tempId_H_Jual = cmd.ExecuteScalar().ToString();

                    for (int i = 0; i < listIdBarang.Count; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            string jenisTemp = "";
                            if (j == 0) jenisTemp = "box";
                            else if (j == 1) jenisTemp = "strip";
                            else if (j == 2) jenisTemp = "biji";

                            int pesananBarang = listQuantity[i, j];
                            if (pesananBarang != 0)
                            {
                                cmd = new OracleCommand("insert into d_jual values('" + tempId_H_Jual + "','" + listIdBarang[i] + "',:qty,'" + jenisTemp + "')", conn);
                                cmd.Parameters.Add(":qty", listQuantity[i, j]);
                                cmd.ExecuteNonQuery();
                            }
                            while (pesananBarang != 0)
                            {
                                bool cekKadaluarsa = false;
                                do
                                {
                                    //cmd = new OracleCommand("select * from d_barang d where d.id_barang='" + listIdBarang[i] + "' and d.expired=(select min(expired) from d_barang where id_barang='" + listIdBarang[i] + "')", conn);
                                    cmd = new OracleCommand("select * from d_barang where jenis='" + jenisTemp + "' and id_barang='" + listIdBarang[i] + "' and expired=(select min(expired) from d_barang where id_barang = '" + listIdBarang[i] + "'and expired > sysdate)", conn);
                                    OracleDataReader reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        if (Convert.ToDateTime(reader["expired"]) <= DateTime.Now)
                                        {
                                            cmd = new OracleCommand("delete from d_barang where jenis='" + jenisTemp + "' and id_barang='" + reader["id_barang"].ToString() + "' and expired=to_date('" + Convert.ToDateTime(reader["expired"]).ToShortDateString().ToString() + "','DD-MM-YYYY')", conn);
                                            cmd.ExecuteNonQuery();
                                            cekKadaluarsa = true;
                                        }
                                        else
                                        {
                                            cekKadaluarsa = false;
                                            if (Convert.ToInt32(reader["stock"]) > pesananBarang)
                                            {
                                                cmd = new OracleCommand("update d_barang set stock=:stock where jenis='" + jenisTemp + "' and id_barang='" + reader["id_barang"].ToString() + "' and expired=to_date('" + Convert.ToDateTime(reader["expired"]).ToShortDateString().ToString() + "','DD-MM-YYYY')", conn);
                                                cmd.Parameters.Add(":stock", (Convert.ToInt32(reader["stock"]) - pesananBarang));
                                                cmd.ExecuteNonQuery();
                                                pesananBarang = 0;
                                            }
                                            else if (Convert.ToInt32(reader["stock"]) < pesananBarang)
                                            {
                                                pesananBarang = pesananBarang - Convert.ToInt32(reader["stock"]);
                                                cmd = new OracleCommand("delete from d_barang where jenis='" + jenisTemp + "' and id_barang='" + reader["id_barang"].ToString() + "' and expired=to_date('" + Convert.ToDateTime(reader["expired"]).ToShortDateString().ToString() + "','DD-MM-YYYY')", conn);
                                                cmd.ExecuteNonQuery();
                                            }
                                            else
                                            {//bila sama
                                                cmd = new OracleCommand("delete from d_barang where jenis='" + jenisTemp + "' and id_barang='" + reader["id_barang"].ToString() + "' and expired=to_date('" + Convert.ToDateTime(reader["expired"]).ToShortDateString().ToString() + "','DD-MM-YYYY')", conn);
                                                cmd.ExecuteNonQuery();
                                                pesananBarang = 0;
                                            }
                                        }
                                    }
                                } while (cekKadaluarsa);
                            }//while pesanan != 0
                        }

                    }
                    MessageBox.Show("Success!!!");
                    ot.Commit();
                }
                catch (Exception ex)
                {
                    ot.Rollback();
                    MessageBox.Show(ex.Message.ToString());
                }

                
                conn.Close();

                //open invoice
                conn.Open();
                //cari max nota
                cmd = new OracleCommand("select max(nota_jual) from h_jual", conn);
                string idNotaTemp = cmd.ExecuteScalar().ToString();

                //cari nama kasir
                cmd = new OracleCommand("select nama from account where username='" + username + "'", conn);
                string namaKasirTemp = cmd.ExecuteScalar().ToString();
                conn.Close();
                ((Form1)MdiParent).openInvoice(namaKasirTemp, idNotaTemp);
            }
        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {//tombol logout
            ((Form1)MdiParent).login();
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {//harga per piece
            if (comboBox1.SelectedIndex != -1 && mulai)
            {
                textBox1.Text = "Rp. " + listHarga[comboBox1.SelectedIndex].ToString() + ",-";
                refreshNumericUD1();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedIndex != -1 && mulai)numericUpDown2.Maximum = Convert.ToInt32(comboBox2.SelectedValue);
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            //((Form1)MdiParent).openInvoice(username, "NJ004");
        }

        public void refreshNumericUD1() {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select count(*) from d_barang db where id_barang='" + comboBox1.SelectedValue + "' and jenis='" + comboBox3.Text + "'", conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar());
            if (temp != 0)
            {
                cmd = new OracleCommand("select sum(db.stock) from d_barang db where id_barang='" + comboBox1.SelectedValue + "' and jenis='" + comboBox3.Text + "'", conn);
                numericUpDown1.Maximum = Convert.ToInt32(cmd.ExecuteScalar());
            }
            else numericUpDown1.Maximum = 0;
            conn.Close();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex != -1 && mulai)
            {
                refreshNumericUD1();
            }
        }
    }
}
