﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectPCS
{
    public partial class formInvoice : Form
    {
        string cashier = "", nonota = "";
        public formInvoice(string cashier, string nonota)
        {
            InitializeComponent();
            this.cashier = cashier;
            this.nonota = nonota;
        }

        private void crystalReportViewer1_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).Location = new Point(350, 0);
            ((Form1)MdiParent).Size = new Size(820, 691);
            this.Close();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void formInvoice_Load(object sender, EventArgs e)
        {
            this.Size = new Size(1366, 768);
            this.Location = new Point(0, 0);
            //this.BackgroundImage = Image.FromFile("login.jpg");
            //this.BackgroundImageLayout = ImageLayout.Stretch;

            //crystal reportnya
            crystalReportViewer1.Location = new Point(81, 164);
            crystalReportViewer1.Size = new Size(640, 430);

            Invoice c = new Invoice();
            c.SetDatabaseLogon("project", "123");
            c.SetParameterValue("NamaKasir", cashier);
            c.SetParameterValue("NotaJual", nonota);
            crystalReportViewer1.ReportSource = c;
            crystalReportViewer1.Refresh();
        }
    }
}
