﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace projectPCS
{
    public partial class login : Form
    {
        int pilihMenu = 0;
        OracleConnection conn = new OracleConnection("Data Source=sby;User ID=project;Password=123;");
        Panel l = new Panel();
        
        List<string> listUsername = new List<string>();
        List<string> listPassword = new List<string>();
        List<string> listStatus = new List<string>();
        public login()
        {
            InitializeComponent();
        }

        private void login_Load(object sender, EventArgs e)
        {
            this.Size = new Size(1366, 768);
            this.Location = new Point(0, 0);
            this.BackgroundImage = Image.FromFile("login.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            textBox1.Size = new Size(300,45);
            textBox1.Location = new Point(837, 338);
            textBox1.Font = new Font("arial",25);
            textBox2.Size = new Size(300, 45);
            textBox2.Location = new Point(837, 450);
            textBox2.Font = new Font("arial", 25);

            panel1.Size = new Size(223, 40);
            panel1.Location = new Point(874, 533);
            panel1.BackColor = Color.Transparent;
            panel2.Size = new Size(223, 40);
            panel2.Location = new Point(874, 601);
            panel2.BackColor = Color.Transparent;

            //radioButton1.BackColor = Color.Transparent;
            //radioButton1.AutoSize = false;
            //radioButton1.Size = new Size(100, 100);
            daftarNama();
        }

        public void daftarNama()
        {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from account", conn);
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                listUsername.Add(reader["username"].ToString());
                listPassword.Add(reader["password"].ToString());
                listStatus.Add(reader["status"].ToString());
            }
            conn.Close();
        }

        private void login_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.X + " " + e.Y);
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 1)
            {
                pilihMenu = 1;
                this.Invalidate();
            }
        }

        private void login_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush = new SolidBrush(Color.FromArgb(64, Color.Gray));
            if (pilihMenu == 1) g.FillRectangle(brush, 874, 533, 223, 40);
            else if (pilihMenu == 2) g.FillRectangle(brush, 874, 601, 223, 40);
        }

        private void login_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 0)
            {
                pilihMenu = 0;
                this.Invalidate();
            }
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            if (textBox1.Text == "" && textBox2.Text == "")
            {
                ((Form1)MdiParent).openMaster();
                this.Close();
            }
            else
            {
                bool cekLogin = false;
                for (int i = 0; i < listUsername.Count; i++)
                {
                    if (textBox1.Text == listUsername[i] && textBox2.Text == listPassword[i] && listStatus[i] == "K")
                    {
                        MessageBox.Show("login Cashier!!!");
                        this.Controls.Remove(l);
                        cekLogin = true;

                        ((Form1)MdiParent).openCashier(listUsername[i]);
                        this.Close();
                        break;
                    }
                    else if (textBox1.Text == listUsername[i] && textBox2.Text == listPassword[i] && listStatus[i] == "C")
                    {
                        MessageBox.Show("login Customer!!!");
                        this.Controls.Remove(l);
                        cekLogin = true;

                        ((Form1)MdiParent).openCustomer(listUsername[i]);
                        this.Close();
                        break;
                    }
                    else if (textBox1.Text == listUsername[i] && textBox2.Text == listPassword[i] && listStatus[i] == "P") {
                        MessageBox.Show("login Employee!!!");
                        this.Controls.Remove(l);
                        cekLogin = true;

                        ((Form1)MdiParent).openEmployee(listUsername[i]);
                        this.Close();
                        break;
                    }
                }
                if (!cekLogin)
                {
                    l = new Panel();
                    l.BackgroundImage = Image.FromFile("wrong.png");
                    l.BackgroundImageLayout = ImageLayout.Stretch;
                    l.BackColor = Color.Transparent;
                    l.Size = new Size(40, 40);
                    l.Location = new Point(1117, 533);
                    this.Controls.Add(l);
                }
            }
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            ((Form1)MdiParent).register("Customer");
            this.Close();
        }

        private void panel2_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 2)
            {
                pilihMenu = 2;
                this.Invalidate();
            }
        }
    }
}
