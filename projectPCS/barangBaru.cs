﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace projectPCS
{
    public partial class barangBaru : Form
    {
        OracleConnection conn = new OracleConnection("Data Source=orcl;User ID=project;Password=123;");
        int pilihMenu = 0;
        public barangBaru()
        {
            InitializeComponent();
        }

        private void barangBaru_Load(object sender, EventArgs e)
        {
            this.Size = new Size(800, 606);
            this.Location = new Point(0, 0);
            this.BackgroundImage = Image.FromFile("input stock.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            //input
            textBox1.Location = new Point(321, 209); //nama barang
            textBox1.Size = new Size(279, 23);
            comboBox1.Location = new Point(321, 259); //nama supplier
            comboBox1.Size = new Size(279, 23);
            textBox2.Location = new Point(321, 309); //fungsi
            textBox2.Size = new Size(279, 24);
            numericUpDown1.Location = new Point(321, 359);
            numericUpDown1.Size = new Size(279, 23);
            numericUpDown2.Location = new Point(321, 409);
            numericUpDown2.Size = new Size(279, 23);

            comboBox1.Font = new Font("arial", 13);
            textBox1.Font = new Font("arial", 13);
            textBox2.Font = new Font("arial", 13);
            numericUpDown1.Font = new Font("arial", 13);
            numericUpDown2.Font = new Font("arial", 13);

            //panel 1 buat back kembali ke stock
            panel1.Size = new Size(30, 40);
            panel1.Location = new Point(735, 38);
            panel1.BackColor = Color.Transparent;

            //buat submit
            panel2.Size = new Size(132, 22);
            panel2.Location = new Point(430, 461);
            panel2.BackColor = Color.Transparent;

            //buat tambah supplier
            panel3.Size = new Size(22, 22);
            panel3.Location = new Point(578, 461);
            panel3.BackColor = Color.Transparent;

            label1.BackColor = Color.Transparent;
            label1.Font = new Font("arial", 13);
            label1.ForeColor = Color.White;
            label1.Location = new Point(610, 461);
            label1.Text = "(New Supplier)";

            isiCb();
        }

        public void isiCb() {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from supplier", conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds,"supplier");

            comboBox1.DataSource = ds.Tables["supplier"];
            comboBox1.DisplayMember = "nama_supplier";
            comboBox1.ValueMember = "id_supplier";

            conn.Close();
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 1)
            {
                pilihMenu = 1;
                this.Invalidate();
            }
        }
        private void panel2_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 2)
            {
                pilihMenu = 2;
                this.Invalidate();
            }
        }
        private void panel3_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 3)
            {
                pilihMenu = 3;
                this.Invalidate();
            }
        }

        private void barangBaru_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 0)
            {
                pilihMenu = 0;
                this.Invalidate();
            }
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {//back to stock
            ((Form1)MdiParent).openStock();
            this.Close();
        }
        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {//submit
            conn.Open();
            //insert barang
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into barang values('','" + textBox1.Text + "','" + textBox2.Text + "',:harga)";
            cmd.Parameters.Add(":harga", (numericUpDown2.Value));
            cmd.ExecuteNonQuery();

            //tampung id_barang
            cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "select id_barang from barang where nama_barang='" + textBox1.Text + "'";
            string temptemp = cmd.ExecuteScalar().ToString();

            //untuk inisialisasi d_barang biar tampilan stocknya keluar
            cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into d_barang values('" + temptemp + "',0,to_date('01-01-0101','DD-MM-YYYY'),'box')";
            cmd.ExecuteNonQuery();
            cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into d_barang values('" + temptemp + "',0,to_date('01-01-0101','DD-MM-YYYY'),'strip')";
            cmd.ExecuteNonQuery();
            cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into d_barang values('" + temptemp + "',0,to_date('01-01-0101','DD-MM-YYYY'),'biji')";
            cmd.ExecuteNonQuery();

            //insert supplier barang
            cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into barang_supplier values('" + comboBox1.SelectedValue + "','" + temptemp + "',:harga)";
            cmd.Parameters.Add(":harga", (numericUpDown1.Value));
            cmd.ExecuteNonQuery();

            conn.Close();
            MessageBox.Show("ADDED!!!");
        }
        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            ((Form1)MdiParent).openInputSupplier();
            this.Close();
        }

        private void barangBaru_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush = new SolidBrush(Color.FromArgb(64, Color.Gray));
            if (pilihMenu == 1) g.FillRectangle(brush, 735, 38, 30, 40);
            else if (pilihMenu == 2) g.FillRectangle(brush, 430, 461, 132, 22);
            else if (pilihMenu == 3) g.FillEllipse(brush, 578, 461, 22, 22);
        }

        private void barangBaru_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.X + " " + e.Y);
        }

    }
}
