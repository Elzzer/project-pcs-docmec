﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectPCS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            login();
            MessageBox.Show("Test");
        }

        public void register(string mode = "") {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 850);
            register l = new register(mode);
            l.MdiParent = this;
            l.Show();
        }
        public void login() {
            this.Location = new Point(50, 0);
            this.Size = new Size(1388, 812);
            login l = new login();
            l.MdiParent = this;
            l.Show();
        }
        public void openMaster()
        {
            this.Location = new Point(50, 0);
            this.Size = new Size(1388, 812);
            AdminPage ap = new AdminPage();
            ap.MdiParent = this;
            ap.Show();
        }

        public void openRegisteredUser()
        {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 850);
            RegisteredUser ru = new RegisteredUser();
            ru.MdiParent = this;
            ru.Show();
        }
        public void openStock()
        {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 850);
            stock s = new stock();
            s.MdiParent = this;
            s.Show();
        }

        public void openBarangBaru() {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 650);
            barangBaru bb = new barangBaru();
            bb.MdiParent = this;
            bb.Show();
        }
        public void openSales() {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 850);
            sales ss = new sales();
            ss.MdiParent = this;
            ss.Show();
        }

        public void openShop() {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 691);
            shop sh = new shop();
            sh.MdiParent = this;
            sh.Show();
        }
        public void openInputSupplier()
        {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 550);
            inputSupplier ins = new inputSupplier();
            ins.MdiParent = this;
            ins.Show();
        }

        public void openCashier(string username)
        {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 691);
            cashier c = new cashier(username);
            c.MdiParent = this;
            c.Show();
        }

        public void openCustomer(string username) {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 850);
            customer cc = new customer(username);
            cc.MdiParent = this;
            cc.Show();
        }

        public void openEmployee(string username) {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 850);
            employee em = new employee(username);
            em.MdiParent = this;
            em.Show();
        }

        public void openConvert() {
            this.Location = new Point(350, 0);
            this.Size = new Size(820, 645);
            convert co = new convert();
            co.MdiParent = this;
            co.Show();
        }

        public void openInvoice(string cashier, string nonota) {
            this.Location = new Point(50, 0);
            this.Size = new Size(1388, 812);
            formInvoice fi = new formInvoice(cashier,nonota);
            fi.MdiParent = this;
            fi.Show();
        }

    }
}
