﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace projectPCS
{
    public partial class inputSupplier : Form
    {
        OracleConnection conn = new OracleConnection("Data Source=orcl;User ID=project;Password=123;");
        int pilihMenu = 0;
        public inputSupplier()
        {
            InitializeComponent();
        }

        private void inputSupplier_Load(object sender, EventArgs e)
        {
            this.Size = new Size(800, 506);
            this.Location = new Point(0, 0);
            this.BackgroundImage = Image.FromFile("input supplier.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;

            textBox1.Location = new Point(321,211);
            textBox1.Size = new Size(278,23);
            textBox1.Font = new Font("arial",14);
            textBox2.Location = new Point(321, 262);
            textBox2.Size = new Size(278, 23);
            textBox2.Font = new Font("arial", 14);
            textBox3.Location = new Point(321, 313);
            textBox3.Size = new Size(278, 23);
            textBox3.Font = new Font("arial", 14);

            //panel 1 buat back kembali ke admin page
            panel1.Size = new Size(30, 40);
            panel1.Location = new Point(735, 38);
            panel1.BackColor = Color.Transparent;

            //panel 2 buat submit supplier baru
            panel2.Size = new Size(112, 22);
            panel2.Location = new Point(321, 367);
            panel2.BackColor = Color.Transparent;

            //panel 1 buat back kembali ke input barang
            panel3.Size = new Size(150, 22);
            panel3.Location = new Point(448, 367);
            panel3.BackColor = Color.Transparent;
        } //598

        private void panel1_MouseHover(object sender, EventArgs e)
        {//buat back ke master page
            if (pilihMenu != 1)
            {
                pilihMenu = 1;
                this.Invalidate();
            }
        }

        private void panel2_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 2)
            {
                pilihMenu = 2;
                this.Invalidate();
            }
        }

        private void panel3_MouseHover(object sender, EventArgs e)
        {//back ke input barang / barang baru
            if (pilihMenu != 3)
            {
                pilihMenu = 3;
                this.Invalidate();
            }
        }

        private void inputSupplier_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 0)
            {
                pilihMenu = 0;
                this.Invalidate();
            }
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            ((Form1)MdiParent).openMaster();
            this.Close();
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            ((Form1)MdiParent).openBarangBaru();
            this.Close();
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            conn.Open();
            OracleCommand cmd = new OracleCommand("insert into supplier values('','" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "')", conn);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Insert Berhasil!!!");
            conn.Close();
        }

        private void inputSupplier_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush = new SolidBrush(Color.FromArgb(64, Color.Gray));
            if (pilihMenu == 1) g.FillRectangle(brush, 735, 38, 30, 40);
            else if (pilihMenu == 2) g.FillRectangle(brush, 321, 367, 112, 22);
            else if (pilihMenu == 3) g.FillRectangle(brush, 448, 367, 150, 22);
        }

        private void inputSupplier_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.X + " " + e.Y);
        }
    }
}
